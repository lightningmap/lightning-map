import matplotlib.pyplot as plt
import scipy.signal as signal
import scipy.constants as constants
import math
import numpy as np
import pickle
import pandas as pd
import sys
import scipy.io as sio


class Data:
    def __init__(self):
        self.data_range = []
        self.channel_b = pd.DataFrame([])
        self.channel_a = pd.DataFrame([])
        self.channel_c = pd.DataFrame([])
        self.peaks = []

    def get_data(self, data_path):
        print('Opening data file...')
        mat_contents = sio.loadmat(data_path)
        print('Data file opened.')
        if not self.data_range:
            self.data_range = [0, len(mat_contents['A']) - 1]

        self.channel_b = pd.DataFrame(mat_contents['A'][self.data_range[0]:self.data_range[1]])
        self.channel_a = pd.DataFrame(mat_contents['B'][self.data_range[0]:self.data_range[1]])
        self.channel_c = pd.DataFrame(mat_contents['C'][self.data_range[0]:self.data_range[1]])
        print('Data received.')
        del mat_contents

    # диапазон обрабатываемых данных
    # в идеале должен определяться автоматически
    def set_data_range(self, data_range):
        self.data_range = data_range

    # после нахождения "подходящих" окон, они могут хранится в файле
    def get_peaks_from_file(self, path):
        with open(path, 'rb') as fp:
            self.peaks = pickle.load(fp)
        print('Peaks amount: ' + str(len(self.peaks)))

    def save_peaks_to_file(self, path):
        with open(path, 'wb') as fp:
            pickle.dump(self.peaks, fp)


class Processing(Data):
    def __init__(self):
        Data.__init__(self)
        self.correlation_center_width = 10
        self.window_size = 256
        self.window_aliasing = 1.0 / 2.0
        self.half_window = int(self.window_size / 2)
        self.sample_time = 6.4 * 10 ** (-9)

    # временная задержка между двумя окнами
    def get_delay(self, series1, series2):
        interval_center = self.window_size
        width = self.correlation_center_width
        corr = signal.correlate(series1, series2, method='fft')
        # если максимум корреляции находится слишком далеко от центра
        # отбрасываем такое событие сразу
        max_corr = np.argmax(corr)
        if interval_center - width > max_corr or max_corr > interval_center + width:
            return None
        # upsampling
        interpolated = corr[interval_center - width:interval_center + width]
        res = signal.resample(interpolated, len(interpolated) * 2)
        argmax = np.argmax(res)
        # если максимум находится на границе, то отбрасываем
        # потому что не получится построить параболу
        if argmax == 0 or argmax >= (2 * width - 2):
            return None
        parabola = np.polyfit(np.arange(argmax / 2.0 - 0.5, argmax / 2.0 + 1, 0.5), res[argmax - 1:argmax + 2], 2)
        return -parabola[1] / (2 * parabola[0]) - width

    def get_closure_delay(self, series1, series2, series3):
        d1 = self.get_delay(series1, series2)
        d2 = self.get_delay(series3, series2)
        d3 = self.get_delay(series1, series3)
        if d1 is None or d2 is None or d3 is None:
            return None
        return d1 + d2 - d3

    def find_peaks(self):
        window_size = self.window_size
        shift = int(window_size * self.window_aliasing)

        # для progressbar
        total = int(math.ceil(len(self.channel_a) / shift)) - 2
        point = int(total / 100)
        increment = int(total / 20)
        for i in range(2, total):
            # progressbar
            if i % (5 * point) == 0:
                sys.stdout.write("\rPeaks search: [" + "=" * int(math.ceil((i / increment))) + " " * int(
                    ((total - i) / increment)) + "]" + str(
                    int(i / point)) + "%")
                sys.stdout.flush()
            step = i * shift
            window1 = np.array(self.channel_a[(step - shift):(step + shift)])
            window2 = np.array(self.channel_b[(step - shift):(step + shift)])
            window3 = np.array(self.channel_c[(step - shift):(step + shift)])
            # иногда нулевой уровень отличается от нуля
            window1 = self.to_zero_level(window1)
            window2 = self.to_zero_level(window2)
            window3 = self.to_zero_level(window3)
            closure_delay = self.get_closure_delay(window1, window2, window3)
            # временный костыль для отсеивания окон с шумом
            amp_condition = max(abs(window1)) > 0.008 and max(abs(window2)) > 0.008 and max(abs(window3)) > 0.008
            # в массив заносим центр (индекс массива исходных данных) окна с пиком
            if closure_delay is not None and abs(closure_delay) < 5 and amp_condition:
                self.peaks.append(step)
        print()
        print('Peaks: ' + str(len(self.peaks)))
        return self.peaks

    # получить окно по заданной координате его центра
    def get_window(self, number):
        peaks = self.peaks
        half_window = int(self.window_size / 2)
        a = self.channel_a[peaks[number] - half_window:peaks[number] + half_window]
        b = self.channel_b[peaks[number] - half_window:peaks[number] + half_window]
        c = self.channel_c[peaks[number] - half_window:peaks[number] + half_window]
        window1 = np.array(a)
        window2 = np.array(b)
        window3 = np.array(c)
        return window1, window2, window3
    
    @staticmethod
    def to_zero_level(series):
        avg = sum(series) / float(len(series))
        return series - avg

    # из методички Stock
    def cos_alpha(self, t1, t2, d):
        t1 = t1 * self.sample_time
        t2 = t2 * self.sample_time
        Az = math.atan(t1 / t2)
        if abs(constants.c / d * math.sqrt(t1 ** 2 + t2 ** 2)) > 1:
            return None
        El = math.acos(constants.c / d * math.sqrt(t1 ** 2 + t2 ** 2))
        return math.sin(Az) * math.cos(El)

    # из методички Stock
    def cos_betta(self, t1, t2, d):
        t1 = t1 * self.sample_time
        t2 = t2 * self.sample_time
        Az = math.atan(t1 / t2)
        if abs(constants.c / d * math.sqrt(t1 ** 2 + t2 ** 2)) > 1:
            return None
        El = math.acos(constants.c / d * math.sqrt(t1 ** 2 + t2 ** 2))
        return math.cos(Az) * math.cos(El)

    # получаем координаты для карты молнии
    def get_map_coordinates(self):
        x = []
        y = []
        series1 = self.channel_a
        series2 = self.channel_b
        series3 = self.channel_c
        half_window = self.half_window
        for i in range(0, len(self.peaks)):
            peak = self.peaks[i]
            window1 = np.array(series1[(peak - half_window):(peak + half_window)])
            window2 = np.array(series2[(peak - half_window):(peak + half_window)])
            window3 = np.array(series3[(peak - half_window):(peak + half_window)])
            window1 = self.to_zero_level(window1)
            window2 = self.to_zero_level(window2)
            window3 = self.to_zero_level(window3)
            t1 = self.get_delay(window1, window2)
            t2 = self.get_delay(window3, window2)
            x.append(self.cos_alpha(t1, t2, 13))
            y.append(self.cos_betta(t1, t2, 13))
        bad_coord = []
        good_coord = []
        # некоторые координаты отсеиваются
        # потому что подразумевают, что cos > 1 в функциях cos_alpha, cos_betta
        for i in range(0, len(x) - 1):
            if x[i] is None:
                bad_coord.append(i)
            else:
                good_coord.append(i)
        print('Bad coordinates: ' + str(bad_coord))
        print('Good coordinates: ' + str(good_coord))
        return x, y


class Main(Processing):
    def __init__(self):
        Processing.__init__(self)
        self.peak_number = 0

    # указываем номер окна, с которым работают остальные
    # функции, которые рисуют различные графики
    def set_peak_number(self, number):
        self.peak_number = number

    # нарисовать окно по трём каналам в файл
    def plot_window_to_file(self, path):
        series1, series2, series3 = self.get_window(self.peak_number)
        plt.figure()
        plt.subplot(311)
        plt.plot(series1, 'r')
        plt.subplot(312)
        plt.plot(series2, 'r')
        plt.subplot(313)
        plt.plot(series3, 'r')
        plt.savefig(path)

    # нарисовать процесс увеличения частоты
    # и аппроксимации параболой
    # центра корреляции
    def plot_processing_to_file(self, path):
        series1, series2, series3 = self.get_window(self.peak_number)
        interval_center = len(series1)
        width = self.correlation_center_width
        interpolated = signal.correlate(series1, series2)[interval_center - width:interval_center + width]
        res = signal.resample(interpolated, len(interpolated) * 2)
        argmax = np.argmax(res)
        parabola = np.polyfit(np.arange(argmax / 2.0 - 0.5, argmax / 2.0 + 1, 0.5), res[argmax - 1:argmax + 2], 2)
        p = np.poly1d(parabola.ravel())
        plt.figure()
        inlen = len(interpolated)

        x = np.arange(0, inlen, 0.5)
        x3 = np.arange(argmax / 2.0 - 0.5, argmax / 2.0 + 1, 0.1)

        y = res
        x2 = np.arange(0, inlen, 1)
        y2 = interpolated

        plt.plot(x, y, 'r.--', x2, y2, 'b.-', x3, p(x3), 'g-')
        plt.savefig(path)

    # нарисовать карту молнии в файл
    def plot_map_to_file(self, path):
        x, y = self.get_map_coordinates()
        plt.figure(figsize=(6, 6))
        ax = plt.gca()
        plt.plot(x, y, 'r.')
        plt.title('lightning map')
        plt.xlim([-1, 1])
        plt.ylim([-1, 1])
        plt.xlabel(r'$cos(\alpha)$')
        plt.ylabel(r'$cos(\beta)$')
        plt.grid(color='black', linestyle='--', alpha=0.2)
        circle = plt.Circle((0, 0), 1, color='black', fill=False)
        ax.add_artist(circle)
        plt.axes().set_aspect('equal', 'box')
        # plt.axhline(0, color='black')
        # plt.axvline(0, color='black')
        plt.savefig(path)
        return 0

    # нарисовать корреляцию по двум каналам в файл
    def plt_correlation_to_file(self, path):
        series1, series2, series3 = self.get_window(self.peak_number)

        plt.figure()
        corr_a_b = signal.correlate(series1, series2, method='fft')
        corr_c_b = signal.correlate(series3, series2, method='fft')

        plt.figure()
        plt.subplot(211)
        plt.title('A-B correlation. Max at: ' + str(np.argmax(corr_a_b)))
        plt.plot(corr_a_b, 'r')

        plt.subplot(212)
        plt.title('C-B correlation. Max at: ' + str(np.argmax(corr_c_b)))
        plt.plot(corr_c_b, 'r')

        plt.savefig(path)
