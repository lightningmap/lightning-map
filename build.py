from classes import *

Main = Main()

Main.set_data_range([13000000, 20000000])

Main.get_data('data/20171206-0001.mat')

# Main.get_peaks_from_file('peaks')

Main.find_peaks()
Main.save_peaks_to_file('peaks')

Main.plot_map_to_file('img/map.png')

# можем построить различные графики для какого-то конкретного окна
Main.set_peak_number(4)

Main.plot_window_to_file('img/window.png')

Main.plt_correlation_to_file('img/correlation.png')

Main.plot_processing_to_file('img/processing.png')
